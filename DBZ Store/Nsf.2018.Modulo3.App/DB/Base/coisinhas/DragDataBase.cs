﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Base.coisinhas
{
    public class DragDataBase
    {
        public int Salvar(DragDTO drag)
        {
            string script =
                @"INSERT INTO tb_produto
                (
                  nm_produto,
                  vl_preco
                )
                VALUES
                (
                  @nm_produto,
                  @vl_preco

                )";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", drag.Produto));
            parms.Add(new MySqlParameter("vl_preco", drag.Preco));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;



        }


        public List<DragDTO> Consultar(string produto)
        {
            string script =
            @"SELECT *
                 FROM tb_produto
                WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteInsertScript(script, parms);

            List<DragDTO> drag = new List<DragDTO>();

            while (reader.read())
            {
                DragDTO novoproduto = new DragDTO();
                novoproduto.Id = reader.GetInt32("id_produto");
                novoproduto.Produto = reader.GetString("nm_produto");
                novoproduto.Preco = reader.GetDecimal("vl_preco");

                drag.Add(novoproduto);

            }
            reader.Close();

            return drag;
        }















    }



}
